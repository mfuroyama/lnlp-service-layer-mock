import fastify from 'fastify';
import fastifyURLData from '@fastify/url-data';
import fastifyProxy from '@fastify/http-proxy';
import chalk from 'chalk';
import config from './config.js';
import {
    cache,
    capture,
    endpoints,
    input,
    logger,
    proxy,
} from './plugins/index.js';

const { npm_package_version: version = '' } = process.env;
const {
    port,
    upstream,
    mode,
    endpoints: endpointList = [],
    cacheDir,
    captureDir,
    debug,
} = config;

const server = fastify({ logger: false });

server.register(endpoints, { endpointList });
server.register(input, { mode });
server.register(logger, { debug });
server.register(fastifyURLData);
server.register(cache, { cacheDir });
server.register(capture, { captureDir });
server.register(proxy);
server.register(fastifyProxy, { upstream, proxyPayloads: false });

console.log(`${chalk.bold('*** LNLP Service Layer Mock ***')} (v${version})`);
console.log(`Runtime mode: ${chalk.cyan(mode.toUpperCase())}`);
console.log(`Upstream URL: ${chalk.bold.green(upstream)}`);

(async () => {
    try {
        await server.listen({ port });
        console.log(`Listening on port ${chalk.green.bold(port)}`);
        console.log();
        console.log(server.inputHelp);
    } catch (err) {
        console.log(err);
        server.logger.info(err);
        process.exit(1);
    }
})();
