import minimist from 'minimist';
import yaml from 'js-yaml';
import fs from 'fs-extra';

const convertCommandLineValue = (value, type) => {
    if (!value || !type || (typeof value !== 'string')) {
        return value;
    }

    if (type === 'boolean') {
        return value.toUpperCase() !== 'FALSE';
    }

    if (type === 'number') {
        return parseInt(value, 10);
    }

    return value;
};

const processOption = (option = {}, args = {}, context = {}) => {
    const {
        name,
        type,
        commandLine = [],
        defaultValue,
    } = option;

    const commandLineValue = commandLine.find((key) => args[key]);
    if (commandLineValue) {
        return convertCommandLineValue(commandLineValue, type);
    }

    const { configKey = name } = option;
    return (context[configKey] || defaultValue);
};

const processOptions = (options = [], args = {}, context = {}) => options.reduce((map, option) => {
    const { name } = option;
    map[name] = processOption(option, args, context);
    return map;
}, {});

const configFileDef = {
    name: 'configFile',
    commandLine: ['configFile', 'c'],
    defaultValue: './config.yaml',
};

const optionsDef = [{
    name: 'cacheDir',
    commandLine: ['cacheDir', 'm'],
    defaultValue: './cache',
}, {
    name: 'captureDir',
    commandLine: ['captureDir', 'o'],
    defaultValue: './capture',
}, {
    name: 'upstream',
    commandLine: ['upstream', 'u'],
    defaultValue: 'http://localhost',
}, {
    name: 'endpoints',
    defaultValue: [],
}, {
    name: 'port',
    type: 'number',
    commandLine: ['port', 'p'],
    defaultValue: 4000,
}, {
    name: 'mode',
    commandLine: ['mode', 'm'],
    defaultValue: 'passthru',
}, {
    name: 'debug',
    type: 'boolean',
    commandLine: ['debug', 'd'],
    defaultValue: false,
}];

const args = minimist(process.argv.slice(2));

const configFile = processOption(configFileDef, args);
const config = yaml.load(fs.readFileSync(configFile));

const options = processOptions(optionsDef, args, config);

export default options;
