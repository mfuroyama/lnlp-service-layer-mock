import fp from 'fastify-plugin';
import chalk from 'chalk';

class Endpoints {
    constructor() {
        this.endpoints = new Set();
        this.isAllEnabled = true;
    }

    add(endpoint) {
        if (!endpoint) {
            console.log(chalk.yellow('Usage: add <<endpoint>>'));
            return;
        }
        if (this.endpoints.has(endpoint)) {
            return;
        }

        this.isAllEnabled = false;
        console.log(`Adding mock endpoint ${chalk.cyan.bold(endpoint)}`);
        this.endpoints.add(endpoint);
    }

    remove(endpoint) {
        if (!endpoint) {
            console.log(chalk.yellow('Usage: remove <<endpoint>>'));
            return;
        }

        if (!endpoint || !this.endpoints.has(endpoint)) {
            return;
        }

        console.log(`Removing mock endpoint ${chalk.cyan.bold(endpoint)}`);
        this.endpoints.delete(endpoint);
    }

    enableAll() {
        if (this.isAllEnabled) {
            return;
        }

        console.log('Enabling all mocked endpoints');
        this.endpoints.clear();
        this.isAllEnabled = true;
    }

    disableAll() {
        console.log('Disabling all mocked endpoints');
        this.endpoints.clear();
        this.isAllEnabled = false;
    }

    isMockEndpointEnabled(endpoint) {
        if (this.isAllEnabled) {
            return true;
        }
        return this.endpoints.has(endpoint);
    }

    toString() {
        if (this.isAllEnabled) {
            return chalk.bold('All endpoints enabled');
        }

        const endpoints = Array.from(this.endpoints);
        if (endpoints.length === 0) {
            return chalk.bold('No endpoints enabled');
        }

        return endpoints.map((endpoint) => chalk.green.bold(endpoint)).join('\n');
    }
}

const endpoints = async (server, options) => {
    const { endpointList } = options;
    const endpointService = new Endpoints();

    if (Array.isArray(endpointList) && endpointList.length > 0) {
        endpointList.forEach((endpoint) => {
            endpointService.add(endpoint);
        });
    }

    server.decorate('endpoints', endpointService);
};

export default fp(endpoints);
