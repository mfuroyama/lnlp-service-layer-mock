import fp from 'fastify-plugin';
import path from 'path';
import fs from 'fs-extra';

const capture = async (server, options) => {
    const { captureDir } = options;

    const writeCaptureFile = async (opts) => {
        const {
            query = {},
            endpoint,
            data,
            statusCode,
            contentType,
        } = opts;
        const { patientId = 'default' } = query;

        const basePath = path.join(captureDir, endpoint);
        await fs.ensureDir(basePath);
        const fileName = `${Date.now()}-${patientId}.json`;
        const filePath = path.join(basePath, fileName);

        let parsedData = data;
        try {
            parsedData = JSON.parse(data);
        } catch (err) {
            // Fail silently
        }

        await fs.writeJSON(filePath, {
            statusCode,
            contentType,
            data: parsedData,
        }, { spaces: 2 });
    };

    server.decorate('capture', writeCaptureFile);
};

export default fp(capture);
