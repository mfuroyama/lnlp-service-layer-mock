import fp from 'fastify-plugin';
import chalk from 'chalk';
import { parse } from 'fast-querystring';

const proxy = async (server) => {
    server.addContentTypeParser(
        'application/x-www-form-urlencoded',
        { parseAs: 'string' },
        (req, body, done) => {
            req.payload = parse(body);
            done(null, body);
        },
    );

    server.addContentTypeParser(
        'application/json',
        { parseAs: 'string' },
        (req, body, done) => {
            req.payload = JSON.parse(body);
            done(null, req.payload);
        },
    );

    server.addHook('preHandler', async (req, reply) => {
        if (req.mode !== 'mock') {
            return;
        }

        const { query = {} } = req;
        const { path: endpoint } = req.urlData();

        // Check to make sure this endpoint was enabled for mocking
        if (!server.endpoints.isMockEndpointEnabled(endpoint)) {
            server.logger.info(`Mock endpoint ${chalk.green(endpoint)} not enabled, falling back to ${chalk.cyan('PASSTHRU')}`);
            return;
        }

        // Retrieve data from the cache. If this results in a cache hit, we respond with the data,
        // otherwise, we just let the call fall through.
        const cachedData = await server.readCache({ query, endpoint });
        if (!cachedData) {
            server.logger.info('Mock data not found, falling back to PASSTHRU');
            return;
        }

        const {
            statusCode = 200,
            contentType = 'application/json',
            data,
        } = cachedData;

        reply
            .code(statusCode)
            .header('content-type', contentType)
            .send(data);

        return reply;
    });

    server.addHook('onSend', async (req, res, payload) => {
        if (req.mode !== 'capture') {
            return payload;
        }

        const chunks = [];

        // eslint-disable-next-line no-restricted-syntax
        for await (const chunk of payload) {
            chunks.push(chunk);
        }

        const buffer = Buffer.concat(chunks);
        const data = buffer.toString();

        const { query = {} } = req;
        const { statusCode } = res;
        const { path: endpoint } = req.urlData();
        const contentType = res.getHeader('content-type');

        await server.capture({
            query,
            endpoint,
            data,
            statusCode,
            contentType,
        });

        // Return the buffer since the payload Buffer was exhausted while reading
        return buffer;
    });
};

export default fp(proxy);
