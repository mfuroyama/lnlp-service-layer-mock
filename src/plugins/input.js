import fp from 'fastify-plugin';
import chalk from 'chalk';

const message = [];
message.push('=== Commands ===');
message.push(`   ${chalk.bold('capture, c')}: Enable ${chalk.bold('CAPTURE')} mode`);
message.push(`      ${chalk.bold('mock, m')}: Enable ${chalk.bold('MOCK')} mode`);
message.push(`  ${chalk.bold('passthru, p')}: Enable ${chalk.bold('PASSTHRU')} mode`);
message.push(`       ${chalk.bold('add, a')}: ${chalk.bold('MOCK')} mode: Add a mocked endpoint`);
message.push(`    ${chalk.bold('remove, r')}: ${chalk.bold('MOCK')} mode: Remove a mocked endpoint`);
message.push(` ${chalk.bold('enableall, e')}: ${chalk.bold('MOCK')} mode: Enable all mocked endpoints`);
message.push(`${chalk.bold('disableall, d')}: ${chalk.bold('MOCK')} mode: Disable all mocked endpoints`);
message.push(`      ${chalk.bold('list, l')}: ${chalk.bold('MOCK')} mode: List all mocked endpoints`);
message.push(`    ${chalk.bold('status, s')}: Show the current runtime status`);
message.push(`      ${chalk.bold('help, h')}: Show commands`);
message.push(`      ${chalk.bold('quit, q')}: Quit LNLP Service Layer Mock`);

const input = async (server, options = {}) => {
    let { mode = 'passthru' } = options || {};

    process.stdin.on('data', (data) => {
        const [rawCommand, param] = data.toString().split(' ').map((value) => value.trim());
        const command = rawCommand.toLowerCase();

        switch (command) {
            case 'c':
            case 'capture':
                mode = 'capture';
                console.log(`Runtime mode set to ${chalk.cyan.bold('CAPTURE')}`);
                break;
            case 'm':
            case 'mock':
                mode = 'mock';
                console.log(`Runtime mode set to ${chalk.cyan.bold('MOCK')}`);
                break;
            case 'p':
            case 'passthru':
                mode = 'passthru';
                console.log(`Runtime mode set to ${chalk.cyan.bold('PASSTHRU')}`);
                break;
            case 'a':
            case 'add':
                server.endpoints.add(param);
                break;
            case 'r':
            case 'remove':
                server.endpoints.remove(param);
                break;
            case 'e':
            case 'enableall':
                server.endpoints.enableAll();
                break;
            case 'd':
            case 'disableall':
                server.endpoints.disableAll();
                break;
            case 'l':
            case 'list':
                console.log(server.endpoints.toString());
                break;
            case 's':
            case 'status':
                console.log(`Runtime mode: ${chalk.cyan(mode.toUpperCase())}`);
                if (mode === 'mock') {
                    console.log();
                    console.log('== Mocked Endpoints ==');
                    console.log(server.endpoints.toString());
                }
                break;
            case 'h':
            case 'help':
                console.log(server.inputHelp);
                break;
            case 'q':
            case 'quit':
                process.exit(0);
                break;
            default:
                console.log(`${chalk.yellow(`Unrecognized command: ${chalk.bold(rawCommand)}`)}`);
                console.log(server.inputHelp);
        }
    });

    server.decorate('inputHelp', message.join('\n'));
    server.decorateRequest('mode', '');

    server.addHook('onRequest', async (req) => {
        req.mode = mode;
    });
};

export default fp(input);
