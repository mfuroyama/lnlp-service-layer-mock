import fp from 'fastify-plugin';
import path from 'path';
import fs from 'fs-extra';

const cache = async (server, options) => {
    const { cacheDir } = options;

    const getCacheFileName = async (endpoint, fileName) => {
        const basePath = path.join(cacheDir, endpoint);
        const filePath = path.join(basePath, fileName);
        const doesFileExist = await fs.exists(filePath);
        if (doesFileExist) {
            return filePath;
        }

        const defaultFilePath = path.join(basePath, 'default.json');
        return defaultFilePath;
    };

    const readCacheFile = async (opts) => {
        const { query = {}, endpoint } = opts;
        const { patientId = 'default' } = query;

        try {
            const fileName = await getCacheFileName(endpoint, `${patientId}.json`);
            const cachedData = await fs.readJSON(fileName);
            return cachedData;
        } catch (err) {
            console.log(err);
            return null;
        }
    };

    server.decorate('readCache', readCacheFile);
};

export default fp(cache);
