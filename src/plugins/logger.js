import fp from 'fastify-plugin';
import dayjs from 'dayjs';
import chalk from 'chalk';
import { customAlphabet } from 'nanoid';

const nanoid = customAlphabet('1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz', 10);

const formatStatusCode = (statusCode) => {
    if (statusCode >= 400) {
        return chalk.red(statusCode);
    }
    if (statusCode >= 300) {
        return chalk.yellow(statusCode);
    }
    return chalk.green(statusCode);
};

const logger = async (server, options = {}) => {
    const { debug: isDebug = false } = options;

    const log = (...args) => {
        const timestamp = `${chalk.cyan(dayjs().format('YYYY-MM-DD HH:mm'))}:`;
        console.log(timestamp, ...args);
    };

    server.decorateRequest('sessionId', '');
    server.decorateRequest('start', 0);

    server.addHook('onRequest', async (req, res) => {
        req.sessionId = nanoid();
        req.start = process.hrtime.bigint();
        const { method } = req;
        const { path: endpoint, query } = req.urlData();

        const message = [
            `[${chalk.blue.bold(req.sessionId)}]`,
            `(${chalk.yellow(req.mode.toUpperCase())})`,
            chalk.cyan(method.toUpperCase()),
            chalk.bold([endpoint, query].filter(Boolean).join('?')),
        ];

        log(message.join(' '));
    });

    server.addHook('onResponse', async (req, res) => {
        const { method } = req;
        const { statusCode } = res;

        const { path: endpoint } = req.urlData();
        const elapsedTime = Math.round(Number(process.hrtime.bigint() - req.start) / 1e6);

        const message = [
            `[${chalk.blue.bold(req.sessionId)}]`,
            `(${chalk.yellow(req.mode.toUpperCase())})`,
            chalk.cyan(method.toUpperCase()),
            chalk.bold(endpoint),
            chalk.bold(formatStatusCode(statusCode)),
        ];

        if (req.isCached) {
            message.push(`(${chalk.yellow('CACHED')})`);
            // const { query = {}, payload: body = '' } = req;
            // message.push(`[${chalk.cyan.bold(getCachedFileName({ query, body, endpoint }))}]`);
        } else {
            message.push(`(${elapsedTime} ms)`);
        }

        log(message.join(' '));
    });

    server.addHook('onError', async (_req, _res, err) => {
        console.log(err);
    });

    server.decorate('logger', {
        info: log,
        debug: isDebug ? log : () => {},
    });
};

export default fp(logger);
