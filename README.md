 # Mock LNLP Service Layer

## Description

The Mock LNLP Service Layer is a project that provides several functions related to LNLP Service Layer interactions. It acts as a REST proxy to the LNLP Service Layer, records the Service Layer output payload, and mocks response data.

## Installation

To install the project, follow these steps:

1. Clone the repository
2. Navigate to the project directory
3. Run `npm install`

## Usage

1. Configure the Clinical UI to use the application URL as its Service Layer endpoint
2. Run `npm run dev` to run the service

## Configuration

The service can be configured via command line arguments or a configuration file.

### Command-Line Options

The command-line configuration options include:

| Name | Command Line Flags | Description |
| ---- | ------------------ | ----------- |
| Configuration YAML file | `configFile (or -c)` | Path to the configuration file (default: ./config.yaml) |
| Mock data directory | `cacheDir (or -m)` | Path to the cache directory. This is the directory that the application will use to search for mock data payloads in `MOCK` mode (default: ./cache) |
| Recorded data directory | `captureDir (or -o)` | Path to the capture directory. This is where the response payloads are stored while in `RECORD` mode (default: ./capture) |
| Upstream URL | `upstream (or -u)` | Upstream URL (default: http://localhost) |
| Mocked endpoints | `endpoints` | List of endpoints to mock in `MOCK` mode (default: []) |
| Initial runtime mode | `mode (or -m)` | Initial runtime mode. Valid values are `passthru`, `mock` or `capture` (default: `passthru`) |
| Proxy Server TCP port | `port (or -p)` | Proxy server TCP port (default: 4000) |
| Debug mode | `debug (or -d)` | Setting this flag will cause debug log messages to be printed to the console |

### Configuration File

Users can opt to store configuration options in a YAML-based configuration file. The default file should be named `config.yaml` and located in the base directory. However, you can specify a config file by using the `configFile` (or `-c`) command line option.

The application supports the following configuration file parameters:

| Name | Type | Required | Description |
|---|---|---|---|
| `port` | number \| string | false | Proxy server TCP port. (default: 4000) |
| `cacheDir` | string | false | Path to the cache directory. This is the directory that the application will use to search for mock data payloads in `MOCK` mode (default: ./cache) |
| `captureDir` | string | false | Path to the capture directory. This is where the response payloads are stored while in `RECORD` mode (default: ./capture) |
| `upstream` | string | false | Upstream URL (default: http://localhost) |
| `endpoints` | string[] | false | List of endpoints to mock in `MOCK` mode (default: []) |
| `mode` | `passthru` \| `mock` \| `capture` | true | Initial runtime mode (default: `passthru`) |
| `debug` | boolean | false | Setting this flag will cause debug log messages to be printed to the console |

#### Example
```yaml
port: 4000
upstream: https://api.lnlp.hawaiirg.com/
mode: mock
endpoints:
    - /documents/getDocuments
cacheDir: ./cache
captureDir: ./capture
debug: true
```

## Operation

### Runtime Modes
The application can run in several "modes". Each runtime mode supports slightly different behavior:

#### Passthru
In "passthru" mode, the application serves as an HTTP proxy, forwarding requests to the URL specified via the `upstream` parameter. This is the default mode of operation.

#### Capture
In "capture" mode, the application will capture and write response payload data to the `captureDir` directory. The application adds metadata (e.g. HTTP status code and MIME content type) and saves the response payload data as JSON. The JSON files are saved with the following information:

`{{requested endpoint as subdirectory}}`/`{{timestamp}}-{{patient ID or 'default' if no patient was available}}`.json

The application saves the JSON files with the following schema:
| Name | Type | Description |
| --- | --- | --- |
| `statusCode` | number | HTTP status code of the response |
| `contentType` | string | MIME type of the response data |
| `data` | any | Raw response data |

#### Mock
In "mock" mode, the application will attempt to serve stored data located in the `cacheDir` directory, if any data exists, for particular endpoint requests.

The cache data fiiles must be in JSON format, with the following naming convention:

`{{requested endpoint as subdirectory}}`/`{{timestamp}}-{{patient ID or 'default' if no patient was available}}`.json

The cached data JSON files should have the following schema:
| Name | Type | Description |
| --- | --- | --- |
| `statusCode` | number | HTTP status code of the response |
| `contentType` | string | MIME type of the response data |
| `data` | any | Raw response data |

The application will first try to serve patient-specific JSON files for the particular endpoint. If none exist, the application will try to serve `default.json` files. If that doesn't exist, the application will fallback to "passthru" mode for that particular endpoint.

### Commands
While running, the application supports commands, enterable in the terminal:

```
   capture, c: Enable CAPTURE mode
      mock, m: Enable MOCK mode
  passthru, p: Enable PASSTHRU mode
       add, a: MOCK mode: Add a mocked endpoint
    remove, r: MOCK mode: Remove a mocked endpoint
 enableall, e: MOCK mode: Enable all mocked endpoints
disableall, d: MOCK mode: Disable all mocked endpoints
      list, l: MOCK mode: List all mocked endpoints
    status, s: Show the current runtime status
      help, h: Show commands
      quit, q: Quit LNLP Service Layer Mock
```

## Contributing
If you want to contribute to this project, please open an issue or a pull request.

## License
This project is licensed under the MIT License.
